//加载react库
import React from 'react';
//挂载dom
import ReactDOM from 'react-dom';

import './style.css'

//TodoList组件，以大写字母开头的都是组件
import TodoList from './TodoList';

ReactDOM.render(
  //jsx语法
  <React.StrictMode>
    <TodoList />
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
