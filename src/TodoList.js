import React,{Component,Fragment} from 'react';
import TodoItem from './TodoItem';

//父组件通过属性的形式向子组件传递参数
//子组件通过props接收父组件传递过来的参数

class TodoList extends Component {
  constructor(props) {
    super(props)
    this.state = {
      list: [],
      inputVale: ''
    };
    this.handleBtnClick = this.handleBtnClick.bind(this);
    this.handleDelete = this.handleDelete.bind(this)
  }
  //组件挂载 dom渲染完成后执行
  componentDidMount() {

  }
  //组件卸载时
  componentWillUnmount() {

  }

  handleBtnClick() {
    let inputVal
    if (this.input.value == '' || this.input.value.length == 0) {
      inputVal = '你添加了空的元素'
    } else {
      inputVal = this.input.value
    }
    this.setState({
      //ES6语法 展开运算符 ...相当于 'learn react','learn javascript','vue'，
      list: [...this.state.list, inputVal]
    })
    this.input.value = ''
  }
  handleDelete(index) {
    //尽量不要直接修改state里的数据 复制个副本
    const list = [...this.state.list]
    list.splice(index, 1)
    this.setState({
      list: list
    })
  }
  getTodoItems() {
    return (
      this.state.list.map((item, index) => {
        return <TodoItem
          delete={this.handleDelete}
          key={index}
          content={item}
          index={index}
        />
        // return <li key={index} onClick={this.handleLiItem.bind(this, index)}>{item}</li>
      })
    )
  }
  render() {
    return (
      <Fragment>
        <div>
          <input style={{width:180,height:25}} type="text"
            ref={input => this.input = input}
            placeholder="placeholder"
          />
          <button className="btn-color" onClick={this.handleBtnClick}>add</button>
        </div>
        <ul>
          {this.getTodoItems()}
        </ul>
      </Fragment>
    )
  }
}

//导出组件 名为TodoList
export default TodoList;
